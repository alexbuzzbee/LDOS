# LDOS drivers

LDOS supports loadable device drivers in the form of `.SYS` files. These are EXE-format executables with type `ldd`. The code section of a driver is invoked with the following table that provides configuration information to the driver:

```lua
{
  commandLine = "",
  kernVersion = {major: 0, minor: 0, fix: 0},
  platform = "",
  platformVersion = "",
}
```

The `commandLine` is the text following `DEVICE=` in CONFIG.SYS.

The `kernVersion` is the LDOS kernel version as reported by the `getVersion` API call.

The `platform` is the type of the hardware platform. On ComputerCraft, it it `cc`.

The `platformVersion` is the version of the hardware platform. On ComputerCraft, it is the version as provided by `_HOST`. If `_HOST` were "ComputerCraft 1.80 (Minecraft 1.12)", `platformVersion` would be "1.80".

The code section is expected to return a table that provides the primary interface between the driver and the rest of the system:

```lua
{
  drives = { -- Filesystem drives.
    A = {
      open = function(path, mode) end,
      delete = function(path) end,
      copy = function(path, newPath) end,
      move = function(path, newPath) end,
      space = function() end,
    },
  },
  devices = { -- Device virtual files.
    DEV = {
      write = function(data) end,
      read = function() end,
      readAll = function() end,
      readLine = function() end,
      close = function() end,
    },
  },
  events = { -- Events fired by the driver (driver should hook events using the normal API).
    example = function() end,
  }
}
```

LDOS will install the contents of the table into the appropriate internal data structures and invoke the functions provided as needed.
