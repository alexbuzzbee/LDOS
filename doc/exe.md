# LDOS EXE format

LDOS has two main types of executable files: flat COM files, which are just plain Lua programs, and EXE files, which are structured with a header and defined sections. The header is structured as follows:

```lua
--[[ldxe
type
luaVersion
codeSize
dataSize
]]--
```

The sequence `--[[ldxe` at the start of the file identifies it as an EXE and starts a Lua comment, preventing the header from being inadvertently executed.

The `type` is one of the following, and indicates how the file can be handled:

* `ldp`: An LDOS Program. Can be run as a normal program with `executeProgram`. Normally has the extension `.EXE`, and is the most common type of EXE program.
* `ldd`: An LDOS Driver. Loaded with `DEVICE=` in CONFIG.SYS to provide device support. Normally has the extension `.SYS`.

Additional `type`s may be used for other purposes.

The `luaVersion` is a two-digit number identifying the minimum required version of Lua. For example, `51` would indicate that Lua 5.1 or later is required.

The `codeSize` is the size of the code section, from the beginning of the file, including the header.

The `dataSize` is the size of the data section, from the end of the code section.

An LDOS EXE is divided into two sections: the code section and the data section. The file begins with the code section, which technically contains the header. The data section follows the code section. The code and data sections do not need to fill the entire file; there may be a part of the file after the data section ignored by the EXE loader.

The two sections of an LDOS EXE are loaded by a routine in the kernel. This routine passes the contents of the code section to `load()` and returns the resulting function. The data section is simply loaded into memory. The data section is made available in the environment of the code section as the last parameter passed to it. It can be retrieved with the following code:

```lua
exeData = table.remove(table.pack(...))
```
