## Version 0.7.0

This is a major feature release and breaks backwards compatibility. It is also very buggy; consider using 0.6.1 instead.

* Total rebuild of basic I/O; now separated into `IO.SYS`, which provides `A:`-`C:`, `CON`, and `NUL` devices.
* Improve portability by removing most CC-specific calls from kernel (`IO.SYS` handles those functions now).
* New `EXE` executable format.
* Totally new driver model based on `EXE` format.
* New API call: `unparseFilePath`.
* Renamed API calls: `writeTerm` and `readTerm` to `writeCon` and `readCon`.
* Removed API call: `readTermNoEcho`.
* Deprecated API call: `newLine`.
* Lots of bugs added (`COMMAND.COM` is broken now :(), some fixed.

## Version 0.6.1

* A few bugfixes.

## Version 0.6

* Bugfixes.
* New API call: `normalizeFilePath`.

## Version 0.5

* As always, bugfixes.
* New commands: `DEL` (deletes a file), `TYPE` (writes a file to the terminal), and `RESET` (resets the machine).

## Version 0.4.2

This is a bugfix-only release.

* Fix many many more bugs.
* Can do a `DIR` successfully!
* Can run external programs successfully!

## Version 0.4.1

This is a bugfix-only release.

* Fix many many bugs.
* Actually boots into `COMMAND.COM` (although many bugs remain to be fixed)!

## Version 0.4

* New API call: `getDirContents`.
* `execute` now indicates success.
* Some work on the `COMMAND.COM` command interpreter.
* Various bug fixes.

## Version 0.3

* Added a Z: drive, pointing to the ROM.
* `dos("writeTerm")` now doesn't automatically end the line.
* Various bug fixes.

## Version 0.2

* Virtual device files now work.
* The boot drive is now detected automatically.

## Version 0.1

* Everything
