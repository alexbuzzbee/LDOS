std = "lua52"

globals = {
  "_HOST",
  "read",
  "term",
  "fs",
  "os",
  "coroutine",
  "shell",
  "peripheral",
  "unpack",
  "dos",
}

files["src/kernel/"] = {
  globals = {
    "apiCalls",
    "version",
    "platform",
    "platformVersion",
    "drives",
    "devices",
    "configDirectives",
    "events",
    "currentDrive",
    "currentDirs",
    "environment",
    "strSplit",
    "parsePath",
    "unparsePath",
    "normalizePath",
    "convertPath",
    "reverseConvertPath",
    "fopen",
    "fclose",
    "fread",
    "freadall",
    "fwrite",
    "writeCon",
    "fseek",
    "fdelete",
    "fmove",
    "fcopy",
    "fsize",
    "mkdir",
    "dirContents",
    "drvSpace",
    "parseProgram",
    "parseProgramString",
    "loadProgram",
    "executeProgram",
    "installDriver",
    "loadDriver",
    "parseConfig",
  }
}

files["src/io/"] = {
  globals = {
    "kernVersion",
    "platformVersion",
    "drives",
    "devices",
    "events",
    "strSplit",
    "biosOpen",
    "biosClose",
    "biosRewind",
    "biosRead",
    "biosWrite",
    "biosSeek",
  }
}

files["src/shell/"] = {
  globals = {
    "displayPrompt",
    "internalCommands",
    "getCommand",
    "processCommand",
  }
}

unused = false
max_line_length = false
