KERNEL_SOURCES = $(wildcard "src/kernel/*.lua")
IO_SOURCES = $(wildcard "src/io/*.lua")
SHELL_SOURCES = $(wildcard "src/shell/*.lua")

default: rebuild

build: LDOS.SYS IO.SYS COMMAND.COM

rebuild: clean build

clean:
	rm LDOS.SYS
	rm COMMAND.COM
	rm IO.SYS

LDOS.SYS: ${KERNEL_SOURCES}
	cat ${KERNEL_SOURCES} > LDOS.SYS
	luac -p LDOS.SYS
	luacheck LDOS.SYS

IO.SYS: ${IO_SOURCES}
	cat ${IO_SOURCES} > IO.SYS
	luac -p IO.SYS
	luacheck IO.SYS

COMMAND.COM: ${SHELL_SOURCES}
	cat ${SHELL_SOURCES} > COMMAND.COM
	luac -p COMMAND.COM
	luacheck COMMAND.COM
