-- String split function --

-- Copied from http://lua-users.org/wiki/SplitJoin
local function strSplit(str, pat)
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = string.find(str, fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
   table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = string.find(str, fpat, last_end)
   end
   if last_end <= #str then
      cap = string.sub(str, last_end)
      table.insert(t, cap)
   end
   return t
end
