-- Driver initialization --

local function main(info)
  if info.kernVersion.minor < 7 then
    error("kernel too old (" .. info.kernVersion.major .. "." .. info.kernVersion.minor .. "." .. info.kernVersion.fix .. ")")
  end
  if info.platform ~= "cc" then
    error("wrong platform (" .. info.platform .. ")")
  end
  if tonumber(info.platformVersion) < 1.79 then
    error("platform too old (" .. info.platformVersion .. ")")
  end
  kernVersion = info.kernVersion
  platformVersion = info.platformVersion
  term.clear()
  term.setCursorPos(1, 1)
  print("Starting LDOS...")
  return {
    drives = drives,
    devices = devices,
    events = events,
  }
end

return main(...)
