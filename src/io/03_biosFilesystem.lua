-- BIOS filesystem access functions --

local openFiles = {}

local function biosHaveFastIo()
  return tonumber(platformVersion) >= 1.80
end

local function biosOpen(path, mode) -- Open a file in mode r (read), w (write), or a (append).
  table.insert(openFiles, {bh = fs.open(path, mode .. "b"), path = path, mode = mode .. "b"})
  return #openFiles
end

local function biosClose(h) -- Close the file.
  openFiles[h].bh.close()
  openFiles[h] = -1
end

local function biosRewind(h) -- Rewind to the start of the file.
  openFiles[h].bh.close()
  openFiles[h].bh = fs.open(openFiles[h].path, openFiles[h].mode)
end

local function biosRead(h, size)
  if biosHaveFastIo() then -- Try to use fast string read.
    return openFiles[h].bh.read(size)
  end
  local read = ""
  for i = 1, size, 1 do -- Fall back on byte read.
    read = read .. string.char(openFiles[h].bh.read())
  end
  return read
end

local function biosWrite(h, data)
  if biosHaveFastIo() then -- Try to use fast string write.
    return openFiles[h].bh.write(data)
  end
  for i = 1, string.len(data), 1 do -- Fall back on byte write.
    openFiles[h].write(string.byte(data, i))
  end
end

local function biosSeek(h, pos)
  biosRewind(h)
  biosRead(h, pos)
end
