-- Device virtual files --

local conBuffer = ""

devices.CON = {
  write = function(_, data)
    local lines = strSplit(data .. "\n", "\n") -- Final newline oddly gets stripped.
    for i, line in ipairs(lines) do
      local x, y = term.getCursorPos()
      print(line)
      if (not lines[i + 1]) and (string.sub(line, -1) ~= "\n") then
        term.setCursorPos(x + string.len(line), y) -- If the last line doesn't end with a newline, keep the cursor position at the end of the line.
      end
    end
  end,
  read = function(_, size)
    if not size then
      size = math.huge
    end
    while true do -- Read the console until conBuffer is big enough.
      if string.len(conBuffer) >= size then
        local read = string.sub(conBuffer, 1, size)
        conBuffer = string.sub(conBuffer, size + 1)
        return read
      end
      local new = read()
      if new == "" then -- Handle end-of-file.
        local buf = conBuffer
        conBuffer = ""
        return buf
      end
      conBuffer = conBuffer .. "\n" .. new -- Include the newlines in read text.
    end
  end,
  seek = function(_, pos)
    conBuffer = string.sub(conBuffer, pos)
  end,
  ioctl = function(_, op, ...)
    if op == "moveCursor" then
      term.setCursorPos(...)
    elseif op == "clear" then
      term.clear()
    else
      error(op .. ": no such ioctl on CON")
    end
  end,
  close = function()
    conBuffer = ""
  end,
}

devices["CLOCK$"] = { -- Clock device, used to get/set time.
  write = function()
    error("hardware doesn't support setting clock")
  end,
  read = function(_, size) -- Returns the current time in the format "YYYYDDDHHMMSS" where DDD is the day of the year. 24-hour time.
    local epochDay = os.day()
    local year = math.floor(epochDay / 365)
    local day = epochDay % 365

    local preciseHour = os.time()
    local roundedHour = math.floor(preciseHour)
    local minute = math.floor((preciseHour % 1) * 60)
    local second = math.floor((((preciseHour % 1) * 60) % 1) * 60)

    local str = string.format("%04d%03d%02d%02d%02d", year, day, roundedHour, minute, second)

    if string.len(str) ~= 13 then
      error("current time out of bounds")
    end
    return string.sub(str, 1, size)
  end,
  seek = function() end,
  ioctl = function(_, op, ...)
    error("no ioctls available on CLOCK$")
  end,
  close = function() end
}

devices.NUL = {
  write = function() end,
  read = function()
    return ""
  end,
  ioctl = function() end,
  seek = function() end,
  close = function() end,
}
