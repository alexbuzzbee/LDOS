-- Drives --

local function convertPath(drive, pathObj) -- Converts a parsed LDOS path to a BIOS path, when appropriate.
  local biosPath = drive.dir
  for _, dir in ipairs(pathObj.dirs) do
    biosPath = fs.combine(biosPath, dir)
  end
  biosPath = fs.combine(biosPath, pathObj.basename)
  if pathObj.extension ~= "" and pathObj.extension ~= nil then -- Don't go insane on directories.
    biosPath = biosPath .. "."
    biosPath = biosPath .. pathObj.extension
  end
  return biosPath
end

local dirDrive = {}

function dirDrive:open(pathObj, mode)
  local biosPath = convertPath(self, pathObj)
  local f = biosOpen(biosPath, mode)
  return {
    read = function(_, size)
      return biosRead(f, size)
    end,
    write = function(_, data)
      return biosWrite(f, data)
    end,
    seek = function(_, pos)
      return biosSeek(f, pos)
    end,
    close = function(_)
      return biosClose(f)
    end,
  }
end

function dirDrive:delete(pathObj)
  return fs.delete(convertPath(self, pathObj))
end

function dirDrive:copy(oldPathObj, newPathObj)
  return fs.copy(convertPath(self, oldPathObj), convertPath(self, newPathObj))
end

function dirDrive:move(oldPathObj, newPathObj)
  return fs.move(convertPath(self, oldPathObj), convertPath(self, newPathObj))
end

function dirDrive:size(pathObj)
  return fs.getSize(convertPath(self, pathObj))
end

function dirDrive:mkdir(pathObj)
  return fs.makeDir(convertPath(self, pathObj))
end

function dirDrive:space()
  return fs.getFreeSpace(self.dir)
end

function dirDrive:dirContents(pathObj) -- Returns a list of files and directories in `path`.
  local contents = {}
  local biosPath = convertPath(self, pathObj)
  for i, item in ipairs(fs.list(biosPath)) do
    if item == nil then
      item = ""
    end
    local itemBiosPath = fs.combine(biosPath, item) -- Get the full BIOS path to the item.
    local itemLdosPath = dos("unparseFilePath", pathObj) .. string.upper(item) -- Get the full LDOS path to the item.
    local itemLdosPathObj = dos("parseFilePath", itemLdosPath) -- Get the LDOS path object to the item.
    local itemType

    if not string.match(itemBiosPath, "disk%d+") and not string.match(itemBiosPath, "rom") then -- Skip disk/rom directories.
      if fs.isDir(item) then -- Set the item's type.
        itemType = "dir"
      else
        itemType = "file"
      end

      contents[i] = { -- Create a table to represent the item.
        path = itemLdosPath,
        type = itemType,
        name = itemLdosPathObj.basename,
      }

      if itemType == "file" then -- Add extra info about files.
        contents[i].size = fs.getSize(itemBiosPath)
        contents[i].ext = itemLdosPathObj.extension
      end
    end
  end
  return contents
end

local function makeDirDrive(dir)
  local drive = {}
  for i, v in pairs(dirDrive) do
    drive[i] = v
  end
  drive.dir = dir
  return drive
end

drives.A = makeDirDrive("/disk")
drives.B = makeDirDrive("/disk2")
drives.C = makeDirDrive("/")
drives.Z = makeDirDrive("/rom")
