-- Command processing --

local function getCommand()
  displayPrompt()
  local buf = ""
  local new = ""
  while new ~= "\n" do
    buf = buf .. new
    new = dos("readCon", 1)
  end
  return buf
end

local function processCommand(line)
  local words = {}
  local i = 1
  for word in string.gmatch(line, "([^ ]+)") do -- Break the line up into words.
    words[i] = word
    i = i + 1
  end
  local command = string.upper(table.remove(words, 1)) -- Get the command itself.
  local drive = string.match(command, "^([a-zA-Z]):$")
  if drive then -- Change drive commands.
    dos("chDrive", drive)
    return true
  end
  for internal, tab in pairs(internalCommands) do -- Try internal commands.
    if internal == command then
      local status, err = pcall(tab.exec, words) -- Run the internal command.
      if not status then
        dos("writeCon", "Error: " .. err .. "\n")
      end
      return status -- Return the status.
    end
  end
  if not dos("execute", command, unpack(words)) then -- Try running the command as given...
    if not dos("execute", command .. ".COM", unpack(words)) then -- ...as a .COM...
      if not dos("execute", command .. ".EXE", unpack(words)) then -- ...and as a .EXE...
        return false -- ...and fail if none of those work.
      end
    end
  end
  return true -- Succede.
end
