-- Internal commands --

local internalCommands = {
  DIR = { -- Displays a directory listing.
    exec = function(params)
      local files, totalSize, dirs = 0, 0, 0
      if params[1] == nil then
        params[1] = dos("getDir")
      end
      local contents = dos("getDirContents", params[1])
      for _, entry in pairs(contents) do
        if entry.name == nil or entry.name == "" then
          entry.name = "?"
        end
        if entry.ext == nil or entry.ext == "" and entry.type == "file" then
          entry.ext = "?"
        end
        if entry.size == nil then
          entry.size = 0
        end
        if entry.type == "file" then
          dos("writeCon", entry.name .. "\t")
          dos("writeCon", entry.ext .. "\t" .. entry.size .. "\n")
          files = files + 1
          totalSize = totalSize + entry.size
        elseif entry.type == "dir" then
          dos("writeCon", entry.name)
          dos("writeCon", "\t" .. "<DIR>\n")
          dirs = dirs + 1
        else
          dos("writeCon", "\t" .. "<???>\n")
        end
      end
      dos("writeCon", files .. " " .. "File(s)" .. " " .. totalSize .. " Bytes.\n")
      dos("writeCon", dirs .. " " .. "Dir(s)" .. " " .. dos("getSpaceForDrive", dos("parseFilePath", params[1]).drive) --[[Gets free space for the drive containing the dir we're listing]] .. " Bytes free.\n")
    end
  },
  CD = { -- Changes directory on the current drive.
    exec = function(params)
      dos("chDir", params[1])
    end
  },
  DEL = { -- Deletes a file.
    exec = function(params)
      dos("deleteFile", params[1])
    end
  },
  TYPE = { -- Writes the contents of a file on the terminal.
    exec = function(params)
      local f = dos("openFile", params[1])
      while true do
        local line = dos("readFile", f, false, false)
        if line == "" then
          break
        end
        dos("writeCon", line .. "\n")
      end
      dos("closeFile", f)
    end
  },
  RESET = { -- Resets the system.
    exec = function()
      os.reboot()
    end
  },
}
