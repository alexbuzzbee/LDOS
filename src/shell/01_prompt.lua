-- Prompt --

local function displayPrompt()
  dos("writeCon", dos("normalizeFilePath", dos("getDir")) .. "> ")
end
