-- Driver manager --

local function installDriver(func, line)
  local driver = func({
    commandLine = line,
    kernVersion = version,
    platform = platform,
    platformVersion = platformVersion,
  })

  for idx, drive in pairs(driver.drives) do
    drives[idx] = drive
    currentDirs[idx] = "\\"
  end
  for idx, device in pairs(driver.devices) do
    devices[idx] = device
  end
  for idx, event in pairs(driver.events) do
    events[idx] = event
  end
end

local function loadDriver(path, line)
  local type, func = loadProgram(path)
  if type ~= "ldd" then
    error(path .. ": not a driver")
  end

  installDriver(func, line)
end
