-- Program loader --

local function parseProgram(getter, size, name) -- getter is a function returning the specified substring from the program.
  if getter(1, 8) == "--[[ldxe" then -- EXE
    local header = getter(9, 11) -- Header will always be at least 11 bytes.
    local next = 19
    local latest = ""
    while latest ~= "]" do -- Get the entire header (it ends at the ]).
      header = header .. latest
      next = next + 1
      latest = getter(next, 1)
    end
    next, latest = nil, nil

    local type, luaVersion, codeSize, dataSize = table.unpack(strSplit(header, "\n"))

    if tonumber(string.sub(_VERSION, 5, 8)) * 10 < tonumber(luaVersion) then -- Compare the two different version formats.
      error("program " .. name .. " expects more recent Lua version")
    end

    local code = getter(0, codeSize)
    local data = getter(codeSize + 1, dataSize)

    local func, err = load(code, name)
    if func == nil then
      error(err)
    end

    local function start(...) -- Pass the EXE data as the last parameter to the program.
      return func(..., data)
    end

    return type, start, data
  else -- COM
    local func, err = load(getter(0, size), name)
    if func == nil then
      error(err)
    end
    return "com", func
  end
end

local function parseProgramString(content, name)
  return parseProgram(function(start, size)
    return string.sub(content, start, start + size - 1)
  end, string.len(content), name)
end

local function loadProgram(path)
  local pathObj = parsePath(path)
  local f = fopen(path, "r")

  return parseProgram(function(start, size)
    fseek(f, start)
    return fread(f, size)
  end, fsize(path), pathObj.basename .. "." .. pathObj.extension)
end

local function executeProgram(path, args)
  local type, func = loadProgram(path)
  if type ~= "ldp" and type ~= "com" then
    return false, path .. ": not a program"
  end
  return pcall(func, args)
end
