-- Bootstrap --

local function boot(io, bootDrive, _platform, _platformVersion) -- Boots LDOS.
  currentDrive = bootDrive
  environment.BOOTDRIVE = bootDrive
  platform = _platform
  platformVersion = _platformVersion
  local ioType, ioFunc = parseProgramString(io, "IO.SYS")
  if ioType ~= "ldd" then
    return "IO.SYS is not a driver (" .. tostring(ioType) .. ")"
  end
  local result, reason = pcall(installDriver, ioFunc, "IO.SYS")
  if not result then
    return "IO: " .. reason
  end
  parseConfig("\\CONFIG.SYS")
  executeProgram(environment.SHELL)
  return "shell exited"
end

local result, reason = pcall(boot, ...)
if not result then
  return "uncaught error: " .. reason
elseif reason == nil then
  return "unknown error"
else
  return reason
end
