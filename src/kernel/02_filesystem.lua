-- Filesystem manager --

local openFiles = {}

local EXP_FILE = "(.-)%.(.-)$"
local EXP_DRIVE = "^(.):"

-- Copied from http://lua-users.org/wiki/SplitJoin
local function strSplit(str, pat)
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = string.find(str, fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
   table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = string.find(str, fpat, last_end)
   end
   if last_end <= #str then
      cap = string.sub(str, last_end)
      table.insert(t, cap)
   end
   return t
end

local function parsePath(path)
  local drive, finalName, basename, extension
  local elements = {}

  if type(path) ~= "string" then
    error("bad argument: string expected, got " .. type(path))
  end

  elements = strSplit(path, "\\")

  if #elements == 0 then
    return parsePath(currentDrive .. ":\\")
  end

  if elements[1] ~= nil and string.match(elements[1], EXP_DRIVE) then
    drive = string.match(table.remove(elements, 1), EXP_DRIVE)
  else
    drive = currentDrive
  end

  finalName = table.remove(elements)

  if finalName == nil then
    basename, extension = "", nil
  else
    basename, extension = string.match(finalName, EXP_FILE)
  end

  if basename == nil then
    basename = finalName
    extension = nil
  end

  return {
    drive = drive,
    dirs = elements,
    basename = basename,
    extension = extension,
  }
end

local function unparsePath(pathObj, includeDrive) -- Turns a parsed path back into a path string.
  local result = ""
  if includeDrive ~= false then
    result = pathObj.drive .. ":\\"
  end
  for _, dir in ipairs(pathObj.dirs) do
    result = result .. dir .. "\\"
  end
  result = result .. pathObj.basename
  if pathObj.extension then
    result = result .. "." .. pathObj.extension
  end
  return result
end

local function normalizePath(path, includeDrive) -- Turns a path into its 'canonical' representation.
  return unparsePath(parsePath(path), includeDrive)
end

local function fopen(path, mode) -- Opens a file.
  for name, device in pairs(devices) do -- Handle device virtual files.
    if path == name then
      table.insert(openFiles, device)
      return #openFiles
    end
  end
  local pathObj = parsePath(path)
  table.insert(openFiles, drives[pathObj.drive]:open(pathObj, mode)) -- Open the file using the function associated with the drive.
  return #openFiles
end

local function fclose(handle) -- Closes an open file. Returns true for success, false for failure.
  if openFiles[handle] ~= -1 then
    openFiles[handle]:close()
    openFiles[handle] = -1
    return true
  else
    return false
  end
end

local function fread(handle, size) -- Reads a byte from, line from, or all of an open file. Returns the read value, nil for failure.
  if openFiles[handle] ~= -1 then
    return openFiles[handle]:read(size)
  else
    return nil
  end
end

local function freadall(handle) -- Reads the entire contents of an open file. Returns the read value, nil for failure.
  if openFiles[handle] ~= -1 then
    local read = ""
    local next = ""
    repeat
      next = openFiles[handle]:read(512)
      read = read .. next
    until next == ""
    return read
  else
    return nil
  end
end

local function fwrite(handle, data) -- Closes an open file. Returns true for success, false for failure.
  if openFiles[handle] ~= -1 then
    return openFiles[handle]:write(data)
  else
    return false
  end
end

local function writeCon(str)
  if devices.CON then
    devices.CON:write(str)
  end
end

local function fseek(handle, whence)
  if openFiles[handle] ~= -1 then
    openFiles[handle]:seek(whence)
    return true
  else
    return false
  end
end

local function ioctl(handle, op, ...)
  if openFiles[handle] ~= -1 then
    return openFiles[handle]:ioctl(op, ...)
  else
    return false
  end
end

local function fdelete(path) -- Deletes the specified file or directory.
  local pathObj = parsePath(path)
  drives[pathObj.drive]:delete(pathObj)
end

local function fmove(path, newPath) -- Moves the specified file to a new location.
  local pathObj = parsePath(path)
  local newPathObj = parsePath(newPath)
  if pathObj.drive == newPathObj.drive then -- Try to use a fast move within the filesystem.
    drives[pathObj.drive]:move(pathObj, newPathObj)
  else -- Fall back on read-write-delete.
    local f = fopen(path, "r")
    local fn = fopen(newPath, "w")
    fwrite(fn, freadall(f))
    fclose(f)
    fclose(fn)
    fdelete(path)
  end
end

local function fcopy(path, newPath) -- Copies the specified file to a new location.
  local pathObj = parsePath(path)
  local newPathObj = parsePath(newPath)
  if pathObj.drive == newPathObj.drive then -- Try to use a fast copy within the filesystem.
    drives[pathObj.drive]:copy(pathObj, newPathObj)
  else -- Fall back on read-write.
    local f = fopen(path, "r")
    local fn = fopen(newPath, "w")
    fwrite(fn, freadall(f))
    fclose(f)
    fclose(fn)
  end
end

local function fsize(path) -- Returns the size of the specified file, in bytes.
  local pathObj = parsePath(path)
  return drives[pathObj.drive]:size(pathObj)
end

local function mkdir(path) -- Creates a directory.
  local pathObj = parsePath(path)
  drives[pathObj.drive]:mkdir(pathObj)
end

local function dirContents(path) -- Returns a list of files and directories in `path`.
  local pathObj = parsePath(path)
  return drives[pathObj.drive]:dirContents(pathObj)
end

local function drvSpace(drive)
  return drives[drive]:space()
end
