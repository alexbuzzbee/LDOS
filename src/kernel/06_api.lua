-- LDOS API interface (and small parts of the implementation) --

apiCalls = {
  terminate = { -- Terminate the running program.
    called = function()
      events.terminate()
    end
  },
  readCon = { -- Read from the console.
    called = function(size) return devices.CON:read(size) end
  },
  writeCon = { -- Write to the console. Does not add a newline to the end.
    called = writeCon
  },
  newLine = {
    called = function() writeCon("\n") end
  },
  openFile = { -- Open a file.
    called = function(path, mode) return fopen(path, mode) end
  },
  closeFile = { -- Close an opened file.
    called = function(handle) return fclose(handle) end
  },
  readFile = { -- Reads from a file.
    called = function(handle, size) return fread(handle, size) end
  },
  writeFile = { -- Writes to a file.
    called = function(handle, data) return fwrite(handle, data) end
  },
  ioctl = {
    called = function(handle, ...) return ioctl(handle, ...) end
  },
  deleteFile = { -- Delete a file or directory.
    called = function(path) return fdelete(path) end
  },
  moveFile = { -- Move a file.
    called = function(path, newPath) return fmove(path, newPath) end
  },
  copyFile = { -- Copy a file.
    called = function(path, toPath) return fcopy(path, toPath) end
  },
  createDir = { -- Create a directory.
    called = function(path) return mkdir(path) end
  },
  getDirContents = { -- Get a list of files/directories in the current directory.
    called = function(path) return dirContents(path) end
  },
  getFileSize = { -- Returns the size of the specified file.
    called = function(path) return fsize(path) end
  },
  parseFilePath = { -- Parse a file path into drive and path components (using current drive/directory).
    called = function(path) return parsePath(path) end
  },
  unparseFilePath = { -- Convert a parsed file path back into a string.
    called = function(path) return unparsePath(path) end
  },
  normalizeFilePath = { -- Convert a file path into its 'canonical' form.
    called = function(path) return normalizePath(path) end
  },
  chDrive = { -- Change the current drive.
    called = function(new)
      if string.len(new) == 1 then
        currentDrive = string.upper(new)
      end
    end
  },
  getDrive = { -- Returns the current drive.
    called = function() return currentDrive end
  },
  getSpaceForDrive = { -- Returns the amount of free space, in bytes, on the specified drive.
    called = function(drive) return drvSpace(string.upper(drive)) end
  },
  chDirForDrive = { -- Change directory on a given drive.
    called = function(new, drive)
      currentDirs[drive] = new
    end
  },
  chDir = { -- Change directory on the current drive.
    called = function(new) currentDirs[currentDrive] = normalizePath(new, false) end
  },
  getDirForDrive = { -- Returns the current directory on a given drive.
    called = function(drive) return currentDirs[drive] end
  },
  getDir = { -- Returns the current directory on the current drive.
    called = function() return currentDirs[currentDrive] end
  },
  on = { -- Set the handler for an event.
    called = function(name, callback) events[name] = callback end
  },
  getHandler = { -- Returns the current handler for an event.
    called = function(name) return events[name] end
  },
  getDate = { -- Returns the current day.
    called = function() return devices["CLOCK$"]:read(7) end
  },
  getTime = { -- Returns the current time of day.
    called = function() return string.sub(devices["CLOCK$"]:read(13), 8) end
  },
  getVersion = { -- Returns the LDOS version.
    called = function() return version end
  },
  getEnv = { -- Returns the value of the specified environment variable.
    called = function(name) return environment[name] end
  },
  setEnv = { -- Sets an environment variable.
    called = function(name, value) environment[name] = value end
  },
  execute = { -- Executes a program.
    called = executeProgram
  },
}

function _G.dos(callName, ...) -- Calls the LDOS API.
  if apiCalls[callName] ~= nil then
    return apiCalls[callName].called(...)
  else
    error("no such API call: '" .. callName .. "'.")
  end
end
