-- Variables --

local version = {major = 0, minor = 7, fix = 0}
local platform = "UNKNOWN"
local platformVersion = "UNKNOWN"

local apiCalls = {}
local drives = {}

local devices = { -- Device virtual files.
  AUX = {
    write = function() end,
    read = function() return "" end,
    readAll = function() return "" end,
    readLine = function() return "" end,
    seek = function() end,
    close = function() end
  },
}

local configDirectives

local environment = { -- Environment variables. Shared between all programs.
  SHELL = "\\COMMAND.COM", -- The set shell. Started when no other programs are running.
  BOOTDRIVE = "", -- The boot drive.
}

local events = { -- Association of string names to function handlers. Allows hooking of LDOS' internal functionality.
  terminate = function() error("terminate") end, -- When the running program terminates.
  idle = function() end, -- Idle callout.
  error = function() end, -- When an error occurs inside LDOS.
}

local currentDrive = "" -- The current drive.
local currentDirs = {}
