-- CONFIG.SYS parser --

configDirectives = { -- CONFIG.SYS directives.
  DEVICE = { -- Loads a device driver.
    invoke = function(restOfLine)
      local args = strSplit(restOfLine, " ")
      loadDriver(args[1], restOfLine)
    end
  },
  SHELL = { -- Sets the shell.
    invoke = function(restOfLine)
      environment.SHELL = restOfLine
    end
  },
  INSTALL = { -- Executes a program from within CONFIG.SYS
    invoke = function(restOfLine)
      executeProgram(restOfLine)
    end
  },
  SET = { -- Sets an environment variable.
    invoke = function(restOfLine)
      local args = {}
      for arg in string.gmatch(restOfLine, "%s(.*?)|(.*?)%s") do
        table.insert(args, arg)
      end
      environment[args[1]] = table.concat(args, " ", 2) -- Concatinates the arguments together with spaces, then puts it in the specified environment variable.
    end
  },
  AUX = { -- Sets the AUX device.
    invoke = function(restOfLine)
      if devices[restOfLine] ~= nil then
        devices.AUX = devices[restOfLine]
      end
    end
  },
  REM = { -- Allows comments.
    invoke = function() end
  },
}

local function executeConfigDirective(name, restOfLine) -- Executes a parsed configuration directive.
  if configDirectives[name] ~= nil then
    configDirectives[name].invoke(restOfLine)
    return true
  else
    return false, "Invalid configuration directive: '" .. name .. "'."
  end
end

local function parseConfigLine(line) -- Parses a configuration line.
  local name, restOfLine = string.match(line, "(.+)=(.+)")
  if name == nil then
    return false, "Invalid configuration line: '" .. line .. "'."
  end
  return executeConfigDirective(name, restOfLine)
end

local function parseConfigContents(contents) -- Parses the contents of CONFIG.SYS.
  for line in string.gmatch(contents, "(.-)\n") do
    local success, reason = parseConfigLine(line)
    if not success then
      writeCon("Warning: " .. reason)
    end
  end
end

local function parseConfig(path) -- Parses the CONFIG.SYS at the specified path.
  local f = fopen(path, "r")
  local config = freadall(f)
  parseConfigContents(config)
end
